﻿using System.Threading.Tasks;
using Partner.Surround.Organizations;

namespace Partner.Surround.TaskCenter.AntiCorruption
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IOrganizationUnitAppService _organizationUnitAppService;

        public OrganizationService(IOrganizationUnitAppService organizationUnitAppService)
        {
            _organizationUnitAppService = organizationUnitAppService;
        }

        public async Task GetOrganizationListAsync()
        {
            var test = await _organizationUnitAppService.GetAllOrganizationUnitTree();
        }
    }
}
