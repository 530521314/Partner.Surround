﻿using AutoMapper;
using Partner.Surround.Shared;
using Partner.Surround.TaskCenter.DailyTasks;
using Partner.Surround.TaskCenter.DailyTasks.Dto;

namespace Partner.Surround.TaskCenter
{
    internal static class TaskCenterMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DailyTask, DailyTaskDto>()
                .ForMember(d => d.StartTime, options => options.MapFrom(t => t.DateRange.StartTime))
                .ForMember(d => d.EndTime, options => options.MapFrom(t => t.DateRange.EndTime))
                .ForMember(d => d.TaskStateTypeName, options => options.MapFrom(t => Enumeration.FromValue<TaskStateType>(t.TaskState.Id).Name));
        }
    }
}
