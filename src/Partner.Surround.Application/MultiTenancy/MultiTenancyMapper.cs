﻿using Abp.Application.Editions;
using AutoMapper;
using Partner.Surround.MultiTenancy.Editions.Dto;
using Partner.Surround.MultiTenancy.Tenants.Dto;

namespace Partner.Surround.MultiTenancy
{
    internal static class MultiTenancyMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Tenant, TenantDto>();
            configuration.CreateMap<Edition, EditionDto>();
        }
    }
}
