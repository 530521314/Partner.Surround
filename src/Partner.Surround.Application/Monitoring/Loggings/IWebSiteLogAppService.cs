﻿using Abp.Application.Services;
using Partner.Surround.CommonDto;
using Partner.Surround.Loggings.Dto;

namespace Partner.Surround.Loggings
{
    /// <summary>
    /// 网站运行日志应用层服务
    /// </summary>
    public interface IWebSiteLogAppService : IApplicationService
    {
        /// <summary>
        /// 获取最近的一个日志文件
        /// </summary>
        /// <returns></returns>
        GetLatestWebLogsOutput GetLatestWebLogs();

        /// <summary>
        /// 下载所有的日志文件
        /// </summary>
        /// <returns></returns>
        FileDto DownloadWebLogs();
    }
}
