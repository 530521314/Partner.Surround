﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Partner.Surround.Loggings.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
