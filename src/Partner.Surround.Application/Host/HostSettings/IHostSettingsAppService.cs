﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.Host.HostSettings.Dto;

namespace Partner.Surround.Host.HostSettings
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);
    }
}
