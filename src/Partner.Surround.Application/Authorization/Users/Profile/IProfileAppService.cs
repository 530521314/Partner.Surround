﻿using Abp.Application.Services.Dto;
using Partner.Surround.Authorization.Users.Profile.Dto;
using System.Threading.Tasks;

namespace Partner.Surround.Authorization.Users.Profile
{
    public interface IProfileAppService
    {
        Task ChangePhoneNumber(ChangePhoneNumberDto input);

        Task ChangePassword(ChangePasswordDto input);

        Task<GetProfilePictureOutput> GetProfilePicture();

        Task<GetProfilePictureOutput> GetProfilePictureByUser(EntityDto<long> input);

        Task UpdateProfilePicture(UpdateProfilePictureInput input);
    }
}
