﻿using Abp.Authorization;
using AutoMapper;
using Partner.Surround.Authorization.Permissions.Dto;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Roles.Dto;
using Partner.Surround.Authorization.Users;
using Partner.Surround.Authorization.Users.Dto;

namespace Partner.Surround.Authorization
{
    internal static class AuthorizationMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Role, RoleDto>();
            configuration.CreateMap<Permission, RolePermissionDto>();
            configuration.CreateMap<User, UserDto>();
            configuration.CreateMap<User, UserEditDto>();
            configuration.CreateMap<Permission, PermissionDto>();
        }
    }
}
