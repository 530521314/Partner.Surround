﻿using AutoMapper;
using Partner.Surround.Resource.DataDictionaries;
using Partner.Surround.Resource.DataDictionaries.Dto;

namespace Partner.Surround.Resource
{
    internal static class ResourceMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DataDictionaryItem, DataDictionaryItemDto>();
        }
    }
}
