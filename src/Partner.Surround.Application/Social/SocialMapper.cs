﻿using AutoMapper;
using Partner.Surround.Social.Chat;
using Partner.Surround.Social.Chat.Dto;
using Partner.Surround.Social.Friendships;
using Partner.Surround.Social.Friendships.Cache;
using Partner.Surround.Social.Friendships.Dto;

namespace Partner.Surround.Social
{
    internal static class SocialMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
        }
    }
}
