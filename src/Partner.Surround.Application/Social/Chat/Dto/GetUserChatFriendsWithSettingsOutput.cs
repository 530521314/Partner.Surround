﻿using System;
using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using Partner.Surround.Social.Friendships.Dto;

namespace Partner.Surround.Social.Chat.Dto
{
    public class GetUserChatFriendsWithSettingsOutput
    {
        public DateTime ServerTime { get; set; }
        
        public List<FriendDto> Friends { get; set; }

        public GetUserChatFriendsWithSettingsOutput()
        {
            Friends = new EditableList<FriendDto>();
        }
    }
}