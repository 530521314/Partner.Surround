﻿using System.Collections.Generic;

namespace Partner.Surround.Social.Chat.Dto
{
    public class ChatUserWithMessagesDto : ChatUserDto
    {
        public List<ChatMessageDto> Messages { get; set; }
    }
}