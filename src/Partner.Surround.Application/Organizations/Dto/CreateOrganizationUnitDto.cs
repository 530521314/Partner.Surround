﻿using Abp.Application.Services.Dto;

namespace Partner.Surround.Organizations.Dto
{
    public class CreateOrganizationUnitDto
    {
        public long? ParentId { get; set; }

        public string DisplayName { get; set; }
    }
}
