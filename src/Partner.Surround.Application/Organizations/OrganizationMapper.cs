﻿using Abp.Organizations;
using AutoMapper;
using Partner.Surround.Organizations.Dto;

namespace Partner.Surround.Organizations
{
    internal static class OrganizationMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();
        }
    }
}
