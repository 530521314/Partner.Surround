﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Partner.Surround.Admin.Views
{
    public abstract class SurroundViewComponent : AbpViewComponent
    {
        protected SurroundViewComponent()
        {
            LocalizationSourceName = SurroundCoreConsts.LocalizationSourceName;
        }
    }
}
