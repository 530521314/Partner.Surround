﻿using Abp.AutoMapper;
using Partner.Surround.TaskCenter.DailyTasks.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Areas.TaskCenter.Models.DailyTasks
{
    /// <summary>
    /// 日常任务分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedDailyTaskInput))]
    public class GetPagedDailyTaskViewModel : PagedViewModel
    {
        public string FilterText { get; set; }
    }
}
