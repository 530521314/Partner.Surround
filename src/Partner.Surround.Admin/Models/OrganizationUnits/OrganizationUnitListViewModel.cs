﻿using Abp.AutoMapper;
using Partner.Surround.Organizations.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Models.OrganizationUnits
{
    /// <summary>
    /// 组织机构视图模型
    /// </summary>
    [AutoMap(typeof(OrganizationUnitDto))]
    public class OrganizationUnitListViewModel : EntityViewModel<long>
    {
        public long? ParentId { get; set; }

        public string DisplayName { get; set; }

        public bool Checked { get; set; } = false;
    }
}
