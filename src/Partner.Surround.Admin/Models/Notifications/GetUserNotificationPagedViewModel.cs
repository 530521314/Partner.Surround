﻿using Abp.AutoMapper;
using Abp.Notifications;
using Partner.Surround.Notifications.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Models.Notifications
{
    /// <summary>
    /// 消息分页模型
    /// </summary>
    [AutoMapTo(typeof(GetUserNotificationsPagedInput))]
    public class GetUserNotificationPagedViewModel : PagedViewModel
    {
        public UserNotificationState? State { get; set; }

        //扩展...
    }
}
