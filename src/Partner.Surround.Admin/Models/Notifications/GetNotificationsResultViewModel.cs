﻿using Abp.AutoMapper;
using Abp.Notifications;
using Partner.Surround.Notifications.Dto;
using Partner.Surround.Admin.Models.Common;
using System.Collections.Generic;

namespace Partner.Surround.Admin.Models.Notifications
{
    /// <summary>
    /// 用户消息分页结果视图模型
    /// </summary>
    [AutoMapFrom(typeof(GetNotificationsOutput))]
    public class GetNotificationsResultViewModel : ResponseParamPagedViewModel<UserNotification>
    {
        public int UnreadCount { get; set; }

        public GetNotificationsResultViewModel(int totalCount, int unreadCount, IReadOnlyList<UserNotification> notifications)
            : base(totalCount, notifications)
        {
            UnreadCount = unreadCount;
        }
    }
}
