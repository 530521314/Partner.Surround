﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Partner.Surround.Auditing.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Models.AuditLogs
{
    /// <summary>
    /// 审计日志列表分页视图模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedAuditLogsInput))]
    public class GetPagedAuditLogViewModel : PagedViewModel
    {
        public string ServiceName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
