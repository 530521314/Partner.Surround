﻿using Abp.AutoMapper;
using Partner.Surround.Social.Chat.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Models.Chat
{
    /// <summary>
    /// 会话分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedSessionInput))]
    public class GetPagedSessionViewModel : PagedViewModel
    {
    }
}
