﻿using Abp.Authorization;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Users;

namespace Partner.Surround.Authorization
{
    public class AppPermissionChecker : PermissionChecker<Role, User>
    {
        public AppPermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
