﻿using Abp.MultiTenancy;
using Partner.Surround.Authorization.Users;

namespace Partner.Surround.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
