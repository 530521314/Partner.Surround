﻿using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using Partner.Surround.Authorization;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Users;
using Partner.Surround.Configuration;
using Partner.Surround.Features;
using Partner.Surround.Localization;
using Partner.Surround.MultiTenancy;
using Partner.Surround.Notifications;
using Partner.Surround.Social.Chat;
using Partner.Surround.Social.Friendships;
using Partner.Surround.Timing;

namespace Partner.Surround
{
    [DependsOn(
        typeof(AbpZeroCoreModule),
        typeof(AbpAutoMapperModule)
        )]
    public class SurroundCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            SurroundLocalizationConfigurer.Configure(Configuration.Localization);

            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = SurroundCoreConsts.MultiTenancyEnabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            // Add setting providers
            Configuration.Settings.Providers.Add<AppSettingProvider>();

            // Add feature providers
            Configuration.Features.Providers.Add<AppFeatureProvider>();

            // Add notification providers
            Configuration.Notifications.Providers.Add<AppNotificationProvider>();

            // Add permission providers
            Configuration.Authorization.Providers.Add<AppPermissionProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SurroundCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.RegisterIfNot<IChatCommunicator, NullChatCommunicator>();
            IocManager.Resolve<ChatUserStateWatcher>().Initialize();
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }
    }
}
