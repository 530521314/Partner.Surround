﻿using Abp.Domain.Services;

namespace Partner.Surround
{
    public class SurroundCoreServiceBase : DomainService
    {
        protected SurroundCoreServiceBase()
        {
            LocalizationSourceName = SurroundCoreConsts.LocalizationSourceName;
        }
    }
}
