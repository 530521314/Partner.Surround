﻿using System.Threading.Tasks;
using Partner.Surround.Admin.Controllers;
using Shouldly;
using Xunit;

namespace Partner.Surround.Admin.Tests.Controllers
{
    public class HomeController_Tests: SurroundWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            LoginAsDefaultTenantAdmin();

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}