﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Partner.Surround.Storage.Minio
{
    [DependsOn(typeof(SurroundStorageModule))]
    public class SurroundMinioStorageModule : AbpModule
    {
        public override void Initialize()
        {
            var thisAssembly = typeof(SurroundMinioStorageModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);
        }
    }
}