﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Partner.Surround.Storage.Aliyun
{
    [DependsOn(typeof(SurroundStorageModule))]
    public class SurroundAliyunStorageModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SurroundAliyunStorageModule).GetAssembly());
        }
    }
}
